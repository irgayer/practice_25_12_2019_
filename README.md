Создать клиент-серверное приложение, которое представляет собой чат комнату.

Серверное приложение должно быть ассинхронным, хранить всю передаваемую информацию в бд, получать данные и отвечать новыми сообщениями.
Клиентское приложение - WPF, для отправки сообщений другим участникам чата. В нём указывайте имя, сообщение, опционально порт и ip (если не задано, считаем 127.0.0.1 и 3231)
